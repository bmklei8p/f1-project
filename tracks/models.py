from django.db import models

class Track(models.Model):
    official_title = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    pirelli_img = models.ImageField()