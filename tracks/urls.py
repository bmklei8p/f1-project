from django.urls import path

from tracks.views import (TrackCreateView, TrackDeleteView, TrackDetailView,
                          TrackListView, TrackUpdateView)

urlpatterns = [
    path('', TrackListView.as_view(), name='track_list'),
    path('<int:pk>/', TrackDetailView.as_view(), name='track_detail'),
    path('new/', TrackCreateView.as_view(), name='track_new'),
    path('<int:pk>/delete/', TrackDeleteView.as_view(), name='track_delete'),
    path('<int:pk>/edit/', TrackUpdateView.as_view(), name='track_edit'),
]
