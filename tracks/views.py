from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView
from tracks.models import Track
from django.contrib.auth.mixins import LoginRequiredMixin

class TrackListView(ListView):
    model = Track
    template_name = 'tracks/list.html'

    def __str__(self):
        return self.title


class TrackDetailView(DetailView):
    model = Track
    template_name = 'tracks/detail.html'


class TrackUpdateView(LoginRequiredMixin, UpdateView):
    model = Track
    template_name = 'tracks/edit.html'


class TrackCreateView(LoginRequiredMixin, CreateView):
    model = Track
    template_name = 'tracks/new.html'
    success_url = reverse_lazy('tracks_list')


class TrackDeleteView(LoginRequiredMixin, DeleteView):
    model = Track
    template_name = 'tracks/delete.html'
    success_url = reverse_lazy('tracks_list')